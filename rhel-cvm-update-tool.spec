Name:           rhel-cvm-update-tool
Version:        0.2
Release:        2%{?dist}
Summary:        Tools for updating RHEL CVM images

License:        LGPLv2+
URL:            https://gitlab.com/vkuznets/rhel-cvm-update-tool
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python3-devel

Requires:       cryptsetup
Requires:       tpm2-tools
Requires:	efibootmgr
Requires:	util-linux

%description
Tools for updating bootloader and resealing LUKS volume keys for RHEL CVM
images.

%prep
%setup -q -n %{name}

%build

%install
install -m0755 -D -t %{buildroot}%{_sbindir} rhel-cvm-update-tool

%triggerin -- kernel-uki-virt
%{_sbindir}/rhel-cvm-update-tool

%triggerin -- shim-x64
%{_sbindir}/rhel-cvm-update-tool

%triggerpostun -- kernel-uki-virt
%{_sbindir}/rhel-cvm-update-tool

%files
%doc COPYING.LESSER README.md
%{_sbindir}/rhel-cvm-update-tool

%changelog
* Wed Aug 23 2023 Vitaly Kuznetsov - 0.2-2
- Update for RHEL9.3

* Thu Feb 02 2023 Vitaly Kuznetsov - 0.1-1
- Intial package
