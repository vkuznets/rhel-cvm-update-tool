#! /usr/bin/python3

# SPDX-License-Identifier: LGPL-2.1-or-later

import argparse
import base64
import io
import json
import os
import re
import shutil
import subprocess
import sys
import hashlib
import yaml

# Borrowed from https://gitlab.com/berrange/tpm-prevision/
class PEImage:

    # https://learn.microsoft.com/en-us/windows/win32/debug/pe-format
    # https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg

    # Magic bytes 'MZ'
    DOS_SIG = bytes([0x4d, 0x5a])

    # Offset in DOS header where the PE offset live
    DOS_HEADER_FIELD_PE_OFFSET = 0x3c

    # Magic bytes 'PE\0\0'
    PE_SIG = bytes([0x50, 0x45, 0x00, 0x00])

    # Offsets relative to start of PE_SIG
    PE_HEADER_FIELD_MACHINE = 0x4
    PE_HEADER_FIELD_NUMBER_OF_SECTIONS = 0x6
    PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER = 0x14
    PE_HEADER_SIZE = 0x18

    MACHINE_I686 = 0x014c
    MACHINE_X86_64 = 0x8664
    MACHINE_AARCH64 = 0xaa64

    MACHINES = [MACHINE_I686,
                MACHINE_X86_64,
                MACHINE_AARCH64]


    PE32_SIG = bytes([0x0b, 0x01])
    PE32P_SIG = bytes([0x0b, 0x02])

    # Offsets relative to PE_HEADER_SIZE
    PE32X_HEADER_FIELD_SIZE_OF_HEADERS = 0x3c
    PE32X_HEADER_FIELD_CHECKSUM = 0x40
    PE32_HEADER_FIELD_CERT_TABLE = 0x80
    PE32P_HEADER_FIELD_CERT_TABLE = 0x90

    SECTION_HEADER_FIELD_VIRTUAL_SIZE = 0x8
    SECTION_HEADER_FIELD_SIZE = 0x10
    SECTION_HEADER_FIELD_OFFSET = 0x14
    SECTION_HEADER_LENGTH = 0x28

    def __init__(self, path=None, data=None):
        self.path = path
        self.data = data
        self.sections = {}
        self.authenticodehash = self.load()

    def has_section(self, name):
        return name in self.sections

    def get_section_data(self, name):
        if name not in self.sections:
            raise Exception("Missing section '%s'" % name)

        start, end = self.sections[name]
        with open(self.path, 'rb') as fp:
            fp.seek(start)
            return fp.read(end-start)

    def get_authenticode_hash(self):
        return self.authenticodehash

    def load(self):
        if self.path is not None:
            with open(self.path, 'rb') as fp:
                return self.load_fh(fp)
        elif self.data is not None:
            with io.BytesIO(self.data) as fp:
                return self.load_fh(fp)
        else:
            raise Exception("Either path or data is required")

    def load_fh(self, fp):
        dossig = fp.read(2)
        if dossig != self.DOS_SIG:
            raise Exception("Missing DOS header magic %s got %s" % (self.DOS_SIG, dossig))

        fp.seek(self.DOS_HEADER_FIELD_PE_OFFSET)
        peoffset = int.from_bytes(fp.read(4), 'little')
        fp.seek(peoffset)

        pesig = fp.read(4)
        if pesig != self.PE_SIG:
            raise Exception("Missing PE header magic %s at %x got %s" % (
                self.PE_SIG, peoffset, pesig))

        fp.seek(peoffset + self.PE_HEADER_FIELD_MACHINE)
        pemachine = int.from_bytes(fp.read(2), 'little')

        if pemachine not in self.MACHINES:
            raise Exception("Unexpected PE machine architecture %x expected [%s]" %
                            (pemachine, ", ".join(["%x" % m for m in self.MACHINES])))

        fp.seek(peoffset + self.PE_HEADER_FIELD_NUMBER_OF_SECTIONS)
        numOfSections = int.from_bytes(fp.read(2), 'little')

        fp.seek(peoffset + self.PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER)
        sizeOfOptionalHeader = int.from_bytes(fp.read(2), 'little')

        # image loader header follows PE header
        fp.seek(peoffset + self.PE_HEADER_SIZE)

        ldrsig = fp.read(2)
        if pemachine == self.MACHINE_I686:
            wantldrsig = self.PE32_SIG
            certtableoffset = self.PE32_HEADER_FIELD_CERT_TABLE
        else:
            wantldrsig = self.PE32P_SIG
            certtableoffset = self.PE32P_HEADER_FIELD_CERT_TABLE

        if ldrsig != wantldrsig:
            raise Exception("Missing image loader signature %s got %s" % (
                wantldrsig.hex(), ldrsig.hex()))

        fp.seek(peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_SIZE_OF_HEADERS)
        sizeOfHeaders = int.from_bytes(fp.read(4), 'little')

        fp.seek(peoffset + self.PE_HEADER_SIZE + certtableoffset + 4)
        certTableSize = int.from_bytes(fp.read(4), 'little')

        # When hashing the file, we need to exclude certain areas
        # with variable data as detailed in:
        #
        # https://reversea.me/index.php/authenticode-i-understanding-windows-authenticode/
        #
        # so build a list of start/end offsets to hash over
        tohash = [
            # From start of file, to the checksum
            [0, peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM],

            # From after checksum, to the certificate table
            [peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM + 4,
             peoffset + self.PE_HEADER_SIZE + certtableoffset],

            # From after certificate table to end of headers
            [peoffset + self.PE_HEADER_SIZE + certtableoffset + 8,
             sizeOfHeaders],
        ]

        imageDataLength = sizeOfHeaders
        nextSection = peoffset + self.PE_HEADER_SIZE + sizeOfOptionalHeader
        for i in range(numOfSections):
            fp.seek(nextSection)
            name = fp.read(8).decode('ascii').rstrip('\0')

            fp.seek(nextSection + self.SECTION_HEADER_FIELD_OFFSET)
            sectionOffset = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_SIZE)
            sectionSize = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_VIRTUAL_SIZE)
            sectionVirtualSize = int.from_bytes(fp.read(4), 'little')

            if sectionSize != 0:
                tohash.append([sectionOffset, sectionOffset + sectionSize])
                imageDataLength += sectionSize
                self.sections[name] = [sectionOffset, sectionOffset + sectionVirtualSize]
            nextSection += self.SECTION_HEADER_LENGTH


        fileLength = fp.seek(0, 2) - certTableSize

        if imageDataLength < fileLength:
            tohash.append([imageDataLength, fileLength])

        tohash = sorted(tohash, key=lambda r: r[0])

        h = hashlib.new('sha256')
        for area in tohash:
            fp.seek(area[0])
            h.update(fp.read(area[1]-area[0]))
        return h.digest()

def extend(pcr, digest):
    return hashlib.sha256(pcr + digest).digest()

def run_command(cmdargs, sysexit=False, canfail=False, input=None):
    res = subprocess.run(cmdargs, check=False, capture_output=True, text=True, input=input)
    if res.returncode != 0 and not canfail:
        print("%s returned %d, stdout: %s stderr: %s" % (res.args, res.returncode, res.stdout, res.stderr), file=sys.stderr)
        if sysexit:
            sys.exit(1)
        else:
            raise Exception("%s command failed" % cmdargs[0])
    return res

def get_digest(event, bank):
    for digest in event['Digests']:
        if digest['AlgorithmId'] == bank:
            return digest['Digest']
    return None


def measure(ukipath):
    pcr4 = bytearray.fromhex('0000000000000000000000000000000000000000000000000000000000000000')
    uki = PEImage(ukipath)
    # Extract .linux section from the UKI and measure it too if needed
    linux = PEImage(data=uki.get_section_data(".linux"))

    res = run_command(['tpm2_eventlog', '/sys/kernel/security/tpm0/binary_bios_measurements'])
    events = yaml.safe_load(res.stdout)
    for event in events['events']:
        if event['PCRIndex'] == 4 and event['EventType']:
            if event['EventType'] != 'EV_EFI_BOOT_SERVICES_APPLICATION':
                # EV_EFI_ACTION or EV_SEPARATOR, measure as is
                pcr4 = extend(pcr4, bytearray.fromhex(get_digest(event, 'sha256')))
            else:
                b = bytearray.fromhex(event['Event']['DevicePath'])
                if b[0] == 0x2:
                    # most likely shim
                    pcr4 = extend(pcr4, PEImage('/boot/efi/EFI/redhat/shimx64.efi').load())
                elif b[0] == 0x4 and b[1] == 0x4:
                    # most likely UKI
                    pcr4 = extend(pcr4, uki.get_authenticode_hash())
                elif b[0] == 0x4 and b[1] == 0x3:
                    # most likely vmlinux
                    pcr4 = extend(pcr4, linux.get_authenticode_hash())
                else:
                    # unknown event. keep as is and hope for the best.
                    print("Unknown event measured to PCR4: %s" % event, file=sys.stderr)
                    pcr4 = extend(pcr4, bytearray.fromhex(get_digest(event, 'sha256')))

    # convert to hex string
    pcr4 = ''.join('{:02x}'.format(x) for x in pcr4).upper()

    res = run_command(["tpm2_pcrread", "sha256:7"])
    pcr7 = re.search('  7 : 0x(.*)$', res.stdout).group(1)

    return {'pcrs': [4, 7], 'bank': 'sha256', 4: pcr4, 7: pcr7}

def seal(keyslots, key, pcrs, kversion):
    tempdir = None
    token_json = None
    try:
        res = run_command(['mktemp', '-d'])
        tempdir = res.stdout[:-1]

        with open(tempdir + '/volume_key', 'wb') as fd:
            fd.write(key)

        with open(tempdir + '/pcrs.dat', 'wb') as fd:
            for pcr in pcrs['pcrs']:
                fd.write(bytearray.fromhex(pcrs[pcr]))

        # Create PCR policy
        run_command(['tpm2_startauthsession', '-S', tempdir + '/session.dat'])
        run_command(['tpm2_policypcr', '-S', tempdir + '/session.dat', '-l', pcrs['bank'] + ':' + ','.join([str(pcr) for pcr in pcrs['pcrs']]),
                     '-f', tempdir + '/pcrs.dat', '-L', tempdir + '/pcrpolicy.dat'])
        run_command(['tpm2_flushcontext', tempdir + '/session.dat'])

        run_command(['tpm2_flushcontext', '-t'])
        # Always create ECC key
        run_command(['tpm2_createprimary', '-Q', '-C', 'o', '-g', 'sha256', '-G', 'ecc:null:aes128cfb', '-c',
                     tempdir + '/primary.ctx'])
        run_command(['tpm2_create', '-C', tempdir + '/primary.ctx', '-r',
                     tempdir + '/seal.prv', '-u', tempdir + '/seal.pub',
                     '-i', tempdir + '/volume_key', '-L', tempdir + '/pcrpolicy.dat'])
        tpm2blob = open(tempdir + '/seal.prv', 'rb').read() + open(tempdir + '/seal.pub', 'rb').read()
        token_json = {'type': 'systemd-tpm2',
                      'keyslots': keyslots,
                      'tpm2-pin': False,
                      'tpm2-primary-alg': 'ecc',
                      'tpm2-pcrs': pcrs['pcrs'],
                      'tpm2-pcr-bank': pcrs['bank'],
                      'kversion': kversion,
                      'tpm2-policy-hash': ''.join('{:02x}'.format(x) for x in open(tempdir + '/pcrpolicy.dat', 'rb').read()),
                      'tpm2-blob': base64.b64encode(tpm2blob).decode('ascii')
                      }
    finally:
        if tempdir:
            run_command(['rm', '-r', '-f', tempdir], canfail=True)
    return token_json

def unseal(token_json):
    tempdir = None
    key = None
    try:
        res = run_command(['mktemp', '-d'])
        tempdir = res.stdout[:-1]
        # Marshal TPM2B_PRIVATE and TPM2_PUBLIC structures from 'tpm2-blob' into
        # separate files. Note: TPM data is always in big-endian.
        tpm2_data = base64.b64decode(token_json['tpm2-blob'])
        # TPM2B_PRIVATE length is uint16
        tpm2_private_len = tpm2_data[1] + tpm2_data[0] * 256
        with open(tempdir + '/seal.prv', 'wb') as fd:
            fd.write(tpm2_data[0:tpm2_private_len + 2])
        with open(tempdir + '/seal.pub', 'wb') as fd:
            fd.write(tpm2_data[tpm2_private_len + 2:])

        with open(tempdir + '/pcrpolicy.dat', 'wb') as fd:
            fd.write(bytearray.fromhex(token_json['tpm2-policy-hash']))

        run_command(['tpm2_flushcontext', '-t'])
        if token_json['tpm2-primary-alg'] == 'ecc':
            run_command(['tpm2_createprimary', '-Q', '-C', 'o', '-g', 'sha256', '-G', 'ecc:null:aes128cfb', '-c',
                         tempdir + '/primary.ctx'])
        elif token_json['tpm2-primary-alg'] == 'rsa':
            run_command(['tpm2_createprimary', '-Q', '-C', 'o', '-g', 'sha256', '-G', 'rsa', '-c',
                         tempdir + '/primary.ctx'])
        else:
            raise Exception("Unsupported primary algorithm: %s" % token_json['tpm2-primary-alg'])
        run_command(['tpm2_flushcontext', '-t'])
        run_command(['tpm2_load', '-Q', '-C', tempdir + '/primary.ctx', '-u', tempdir + '/seal.pub', '-r',
                     tempdir + '/seal.prv', '-c', tempdir + '/seal.ctx'])
        run_command(['tpm2_flushcontext', '-t'])
        run_command(['tpm2_unseal', '-Q', '-c', tempdir + '/seal.ctx', '-p', 'pcr:%s:%s' %
                     (token_json['tpm2-pcr-bank'], ','.join(str(pcr) for pcr in token_json['tpm2-pcrs'])),
                     '-o', tempdir + '/volume_key'])
        key = open(tempdir + '/volume_key', 'rb').read()
    except Exception as e:
        print("Failed to unseal volume key: %s" % e, file=sys.stderr)
    finally:
        if tempdir:
            run_command(['rm', '-r', '-f', tempdir], canfail=True)
    return key

def get_all_luks_tokens(dev):
    try:
        res = run_command(['cryptsetup', 'luksDump', dev])
        r = re.search(r'Tokens:(.*?)Digests:', res.stdout, flags=re.DOTALL)
        if r is None:
            return
        tokens = re.findall(r'([0-9]*): systemd-tpm2', r.group(1))
        for token_id in tokens:
            res = run_command(['cryptsetup', 'token', 'export', '--token-id', token_id, dev])
            token_json = json.loads(res.stdout)
            yield token_id, token_json
    except Exception as e:
        print("Failed to get volume key for %s: %s" % (args.dev, e), file=sys.stderr)

def get_all_luks_devices():
    try:
        res = run_command(['lsblk', '-o', 'KNAME,FSTYPE', '--json'])
        d = json.loads(res.stdout)
        return ["/dev/%s" % dev['kname'] for dev in d['blockdevices'] if dev['fstype'] == 'crypto_LUKS']
    except Exception as e:
        print("Failed to get all LUKS devices: %s" % e)
    return []

# Don't try unsealing unknown/unsuitable tokens
def is_token_to_unseal(token_json):
     if not 'kversion' in token_json.keys() or token_json['kversion'] == os.uname().release:
         return True
     if 'tpm2-pcrs' in token_json.keys() and (token_json['tpm2-pcrs'] == [7] or token_json['tpm2-pcrs'] == ['7']):
         return True
     return False

def get_volume_key(tokens, args, dev):
    hastpmtokens = False
    for token_id, token_json in tokens:
        hastpmtokens = True
        if is_token_to_unseal(token_json):
            if args.verbose:
                print("Trying to unseal token %s for %s" % (token_id, dev))
            key = unseal(token_json)
            if key:
                if args.verbose:
                    print("Successfully unsealed token %s for %s" % (token_id, dev))
                return (token_json, key)
    if not hastpmtokens:
        return None, None
    raise Exception("Failed to find an unsealable token for %s" % dev)

def kernel_install_token():
    res = run_command(["kernel-install", "inspect"])
    return re.search('KERNEL_INSTALL_ENTRY_TOKEN: (.*)\n', res.stdout).group(1)

def install_uki_name(kver):
    # Since kernel-5.14.0-327.el9 UKIs are named <entry-token-or-machine-id>-<version>.efi
    # see https://bugzilla.redhat.com/show_bug.cgi?id=2187671 for details.
    krel = re.search("^[0-9]+", kver[7:].split('.')[0]).group(0)
    if kver.startswith("5.14.0-") and int(krel) < 327:
        return "vmlinuz-%s-virt.efi" % kver
    else:
        return "%s-%s.efi" % (kernel_install_token(), kver)

def get_all_ukis(args):
    uki_kvers = []
    for kver in os.listdir("/lib/modules"):
        if os.path.isfile("/lib/modules/%s/vmlinuz-virt.efi" % kver):
            uki_kvers.append(kver)
            # Install UKI to args.ukipath if it's not already there
            path = "%s/%s" % (args.ukipath, install_uki_name(kver))
            if not os.path.isfile(path):
                shutil.copyfile("/lib/modules/%s/vmlinuz-virt.efi" % kver, path)
    return uki_kvers

def reseal(args):
    if not args.dev:
        devs = get_all_luks_devices()
        if args.verbose:
            print("LUKS devices to check: ", devs)
    else:
        devs = [args.dev]

    retval = 0
    uki_kvers = get_all_ukis(args)

    for dev in devs:
        try:
            tokens = [(token_id, token_json) for token_id, token_json in get_all_luks_tokens(dev)]
            token_json, key = get_volume_key(tokens, args, dev)
        except Exception as e:
            print("%s" % e, file=sys.stderr)
            retval = 1
            continue
        if key is None:
            if args.verbose:
                print("No TPM2 tokens for %s found, skipping" % dev)
            continue

        if ('tpm2-pcrs' not in token_json) or ('keyslots' not in token_json):
            print("Malformed token for %s: %s" % (dev, token_json), file=sys.stderr)
            retval = 1
            continue

        if args.password:
            print("Volume password for %s (keyslots %s): %s" % (dev, token_json['keyslots'],
                                                                base64.b64encode(key).decode('ascii')))
            continue

        if token_json['tpm2-pcrs'] == [7] or token_json['tpm2-pcrs'] == ['7']:
            print("Volume key for %s is sealed to PCR7 only, skipping resealing" % dev)
            continue

        keyslots = token_json['keyslots']
        present_kvers = [token_json['kversion'] for token_id, token_json in tokens if 'kversion' in token_json.keys()]

        # Add missing tokens for all UKIs
        for kver in uki_kvers:
            if kver in present_kvers:
                continue
            print("Adding token for UKI %s to %s" % (kver, dev))
            try:
                pcrs = measure("%s/%s" % (args.ukipath, install_uki_name(kver)))
                token_json = seal(keyslots, key, pcrs, kver)
                run_command(['cryptsetup', 'token', 'import', dev], input=json.dumps(token_json))
            except Exception as e:
                print("Failed to seal %s key for UKI %s: %s" % (dev, kver, e), file=sys.stderr)
                retval = 1
                continue

        if args.no_delete:
            continue

        # Re-read all tokens for the device as we may have added some
        try:
            tokens = [(token_id, token_json) for token_id, token_json in get_all_luks_tokens(dev)]
        except Exception as e:
            print("%s" % e, file=sys.stderr)
            retval = 1
            continue

        # Remove tokens for all missing UKIs except for the currently running one
        tokens_to_remove = []
        for token_id, token_json in tokens:
            if 'kversion' in token_json.keys():
                if token_json['kversion'] == os.uname().release:
                    if args.verbose:
                        print("Not removing %s token %s for the currently running kernel" % (dev, token_id))
                    continue
                if os.path.isfile("%s/%s" % (args.ukipath, install_uki_name(token_json['kversion']))):
                    if args.verbose:
                        print("Not removing %s token %s for an existing UKI %s" % (dev, token_id, token_json['kversion']))
                    continue
                tokens_to_remove.append(token_id)

        if not args.force and len(tokens_to_remove) == len(tokens):
            print("Refusing to remove ALL tokens for %s (use '--force' to override)" % dev)
            continue

        if tokens_to_remove != []:
            print("Removing tokens %s for %s" % (tokens_to_remove, dev))
        for token_id in tokens_to_remove:
            run_command(['cryptsetup', 'token', 'remove', '--token-id', token_id, dev], canfail=True)
    return retval

def get_efi_dev_part():
    res = run_command(['lsblk', '-o', 'KNAME,MOUNTPOINT', '--json'])
    d = json.loads(res.stdout)
    for bdev in d['blockdevices']:
        if bdev['mountpoint'] == '/boot/efi':
            m = re.search(r'([a-zA-Z]+)(\d+)$', bdev['kname'])
            return ('/dev/%s' % m.group(1), m.group(2))

def get_boot_entries():
    res = run_command(['efibootmgr', '-v', '-u'])
    entries = re.findall('Boot[0-9].*\\\EFI\\\Linux\\\.*.efi', res.stdout)
    r = []
    for entry in entries:
        if entry.endswith("-virt.efi"):
            r.append((entry[4:8], re.search("vmlinuz-(.*)-virt.efi", entry).group(1)))
        else:
            r.append((entry[4:8], re.search("\\\EFI\\\Linux\\\%s-(.*).efi" % kernel_install_token(), entry).group(1)))
    return r

def boot(args):
    try:
        changed = False
        bootdev, bootpart = get_efi_dev_part()
        entries = get_boot_entries()
        kvers = [kver for bootnum, kver in entries]
        uki_kvers = get_all_ukis(args)
        for uki in uki_kvers:
            if uki not in kvers:
                changed = True
                print("Adding %s boot entry" % uki)
                run_command(['efibootmgr', '-d', bootdev, '-p', bootpart, '-c', '-L', uki, '-l',
                             '\\EFI\\redhat\\shimx64.efi', '-u', '\\EFI\\Linux\\%s ' % install_uki_name(uki)])

        entries_to_remove = [bootnum for bootnum, kver in entries if kver not in uki_kvers]
        if len(entries_to_remove) == len(kvers) and uki_kvers == [] and not args.force:
            print("Refusing to remove ALL UKI boot entries! (use '--force' to override)")
        else:
            if entries_to_remove != []:
                print("Removing unneeded boot entries: %s" % entries_to_remove)
            for bootnum in entries_to_remove:
                changed = True
                run_command(['efibootmgr', '-B', '-b', bootnum])

        # No changes, do not touch BootOrder
        if not changed and not args.bootver:
            return 0

        # Re-read boot entries to include the newly added ones
        entries = sorted(get_boot_entries(), key=lambda a: a[1], reverse=True)
        if entries == []:
            print("No UKIs found!", file=sys.stderr)
            return 1

        bootorder = []
        for bootnum, kver in entries:
            if kver != args.bootver:
                bootorder.append(bootnum)
            else:
                bootorder = [bootnum] + bootorder

        print("Setting new boot order: %s" % bootorder)
        run_command(['efibootmgr', '-o', ','.join(bootorder)])

        return 0
    except Exception as e:
        print("Failed to set up boot entries: %s" % e)
    return 1

def check_uki_virt():
    try:
        res = run_command(['efibootmgr', '-v', '-u'])
        current = re.findall("BootCurrent: ([0-9]+)", res.stdout)[0]
        return re.findall("Boot%s.*\\\EFI\\\Linux.*.efi" % current, res.stdout) != []
    except:
        print("The currently booted entry is not kernel-uki-virt")
        return False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="RHEL CVM update tool")
    parser.add_argument('action', choices=['reseal', 'boot', 'both'], default='both',
                        const='both', nargs='?', help='Reseal LUKS volume keys / update boot configuration / do both')
    parser.add_argument('-d', '--dev', help="Handle the specific LUKS device only")
    parser.add_argument('-p', '--password', help="Print sealed passwords only, do not create/remove tokens", action='store_true')
    parser.add_argument('-v', '--verbose', help="Be verbose", action='store_true')
    parser.add_argument('-u', '--ukipath', help="Path to UKI directory", default='/boot/efi/EFI/Linux')
    parser.add_argument('-n', '--no-delete', help="Do not remove tokens and boot entries for missing UKIs", action='store_true')
    parser.add_argument('-f', '--force', help="Force remove tokens and boot entries even when it doesn't look safe", action='store_true')
    parser.add_argument('-b', '--bootver', help="Set next boot to the specificied and not the newest UKI version (`uname -r`)")
    args = parser.parse_args()

    if not check_uki_virt():
        sys.exit(0)

    if args.action in ["reseal", "both"]:
        retval = reseal(args)
        if retval != 0:
            sys.exit(retval)

    if args.action in ["boot", "both"]:
        retval = boot(args)

    if retval == 0:
        print("CVM update succeeded!")
    else:
        print("CVM update failed: %d!" % retval)

    sys.exit(retval)
