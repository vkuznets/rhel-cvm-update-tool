# Update RHEL confidential VM instance to boot with an updated UKI

## Purpose

This tool updates RHEL VM and prepares it to boot with a new Unified Kernel Image:
- UEFI boot variables are updated
- LUKS volume keys bound to TPM and PCR measurements are re-sealed

## Current limitations
- UEFI -> shim -> UKI boot sequence is presumed
- For PCR4 prediction the following sequence is used:
  - EV_EFI_ACTION
  - EV_SEPARATOR
  - shim
  - UKI
  - vmlinuz
- PCR7 is not supposed to change so the currently observed value is used.
- LUKS volume keys need to be unsealable with the currently observed PCR values.

## Dependencies

'Encrypt' phase:
- python3.(?)
- cryptsetup
- tpm2-tools
- lsblk

## TODO
The tool is very lightly tested at this moment, use at your own risk!

